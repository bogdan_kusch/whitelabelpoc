//
//  WhiteLabelConfig.h
//  WhiteLabel
//
//  Created by hlus on 16.03.2020.
//  Copyright © 2020 Facebook. All rights reserved.
//

#ifndef WhiteLabelConfig_h
#define WhiteLabelConfig_h

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface WhiteLabelConfig : NSObject<RCTBridgeModule>

@end

#endif /* WhiteLabelConfig_h */
