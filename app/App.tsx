import React from 'react';
import { SafeAreaView, Text, View } from 'react-native';

import {WhiteLabelConfig} from './whitelabel/WhiteLabelConfig';

import {styles} from './AppTheme'

export const App = () => (
  <SafeAreaView style={styles.screen}>
    <Text style={styles.title}>
      {WhiteLabelConfig.APP_NAME} App
    </Text>
    <View style={styles.container}>
      <View style={styles.greeting}>
        <Text style={styles.greetingText}>{WhiteLabelConfig.GREETING_TEXT}</Text>
      </View>
    </View>
  </SafeAreaView>
);